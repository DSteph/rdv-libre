import "./index.css"
import Alpine from 'alpinejs'
import {init} from './init'
import flatpickr from "flatpickr"
import { French } from "flatpickr/dist/l10n/fr.js"
import "flatpickr/dist/flatpickr.css"
import "flatpickr/dist/themes/confetti.css"


window.alpine = Alpine
Alpine.data('init', init)

flatpickr(Alpine, {
    "locale": French // locale for this instance only
})

Alpine.start()
