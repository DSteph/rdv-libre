export function init() {

    // Je disable la vue mobile qui génère des comportements étranges en test
    const start_picker = flatpickr("#start-picker", {   
        dateFormat: 'd/m/Y',
        locale: 'fr',
        disableMobile: 'true'
    })
    const end_picker = flatpickr("#end-picker", {   
        dateFormat: 'd/m/Y',
        locale: 'fr',
        disableMobile: 'true'
    })



    return {
        token: new URLSearchParams(location.search).get('token'),
        availabilities: new Array(7).fill().map(() => new Array()),
        appointmentToken: {},
        selectedAvailability: "",
        showModal: false,
        showConfirmedApp: false,
        showPickDate: false,
        weekDays: ["Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche"],
        getTokenAndAvailability: function() {
            try {
                fetch(import.meta.env.VITE_BASE_URL + '/' + import.meta.env.VITE_get_token_url + '?token=' + this.token)
                    .then(response => response.json())
                    .then(response => this.appointmentToken = response)
                    .then(appointmentToken => { 
                        const start_date = new Date(appointmentToken.date_start);
                        const end_date = new Date(appointmentToken.date_end);
                        start_picker.config.minDate = start_date;
                        start_picker.config.maxDate = end_date;
                        start_picker.setDate(start_date);
                        end_picker.config.minDate = start_date;
                        end_picker.config.maxDate = end_date;
                        end_picker.setDate(end_date);
                        
                        fetch(import.meta.env.VITE_BASE_URL + '/' + import.meta.env.VITE_get_availabilities_url, {
                            method: 'GET',
                            headers: {
                                'Authorization': 'Bearer ' + appointmentToken.token,
                            }
                        })
                        .then(response => response.json())
                        .then(response => { 
                            response['availabilities'].forEach(availability =>{
                                const dateObj = new Date(availability.date);
                                const weekDay = dateObj.getDay();
                                let dayObj = {
                                    visible: true,
                                    availability: availability
                                }
                                // La fonction getDay() renvoi 0 pour Dimanche et ainsi de suite,
                                // je dois compenser pour que l'array commence sur Lundi
                                if (weekDay == 0) {
                                    this.availabilities[6].push(dayObj);  
                                }
                                else {
                                    this.availabilities[weekDay-1].push(dayObj);
                                }
                            })
                        })
                    })
            }
            catch (error) { 
                alert("Impossible de récupérer les disponibilités, erreur: " + error);
            }
        },
        refresh: function() {
            // Intéressant: le fais de vider les rows de availabilities ne déclenche aucune réaction de l'affichage,
            // i.e : tous les RDV restent affichés malgrès le fait qu'ils n'existent plus dans l'array
            // Pour un départ blank slate, il faut vider complètement availabilities
            this.availabilities = new Array(7).fill().map(() => new Array());
            try {
                fetch(import.meta.env.VITE_BASE_URL + '/' + import.meta.env.VITE_get_availabilities_url, {
                    method: 'GET',
                    headers: {
                        'Authorization': 'Bearer ' + this.appointmentToken.token,
                    }
                })
                .then(response => response.json())
                .then(response => {
                    response['availabilities'].forEach(availability =>{
                        const dateObj = new Date(availability.date);
                        const weekDay = dateObj.getDay();
                        let dayObj = {
                            visible: true,
                            availability: availability
                        }
                        // La fonction getDay() renvoi 0 pour Dimanche et ainsi de suite,
                        // je dois compenser pour que l'array commence sur Lundi
                        if (weekDay == 0) {
                            this.availabilities[6].push(dayObj);  
                        }
                        else {
                            this.availabilities[weekDay-1].push(dayObj);
                        }
                    })
                })
            }
            catch (error) { 
                alert("Impossible de récupérer les disponibilités, erreur: " + error);
            }
        },
        filter: function() {
            if (start_picker.selectedDates[0] && end_picker.selectedDates[0]) {
                // On set l'heure au plus tot/tard possible pour que >= et <= fonctionnent correctement
                start_picker.selectedDates[0].setHours(0, 0, 0)
                end_picker.selectedDates[0].setHours(23, 59, 59)
                // On permute si start > end
                if (start_picker.selectedDates[0] > end_picker.selectedDates[0]){
                    let temp = start_picker.selectedDates[0];
                    start_picker.setDate(end_picker.selectedDates[0]);
                    end_picker.setDate(temp);
                }
                this.showPickDate = false;
                this.availabilities.forEach(row => {
                    row.forEach(availability =>{
                        const comparableAvailabilityDate = new Date(availability.availability.date);
                        if(comparableAvailabilityDate >= start_picker.selectedDates[0] && comparableAvailabilityDate <= end_picker.selectedDates[0]) {
                            availability.visible = true;
                        }
                        else availability.visible = false;
                    })
                })
            } else {
                this.showPickDate = true;
            }
        },
        dateTimeToString: function(date, time) {
            if (!date) {
                return ""
            }

            let locale = null;
            let dateTimeOptions = {
                weekday: "long",
                day:"numeric",
                year: "numeric",
                month: "long",
            };
            // Pour présenter les dates du appointment token, pas besoin des heures
            if (time == 0) {
                // toLocaleString ne met pas les majuscules sur jour/mois en français, on le fait a la main
                locale = new Date(date).toLocaleString("fr-FR", dateTimeOptions);
            } else {
                dateTimeOptions = {...dateTimeOptions, 
                    hour: "numeric",
                    minute: "numeric",
                }
                // toLocaleString ne met pas les majuscules sur jour/mois en français, on le fait a la main
                locale = new Date(date + "T" + time).toLocaleString("fr-FR", dateTimeOptions);
            }

            let words = []

            locale.split(" ").forEach((value, i) => {
                if (i == 0 || i == 2) {
                    words.push(value.charAt(0).toUpperCase() + value.slice(1))
                } else {
                    words.push(value)
                }
            })

            return words.join(" ");
        },
        createAppointment: function() {
            try {
                fetch(import.meta.env.VITE_BASE_URL + '/' + import.meta.env.VITE_create_appointment_url,
                { 
                    method: 'POST',
                    body: JSON.stringify(this.selectedAvailability),
                    headers: {
                        'Authorization': 'Bearer ' + this.appointmentToken.token,
                        'Content-Type': 'application/json',
                    }
                }).then(response => response.text()).then(this.showConfirmedApp = true)
            }
            catch (error) { 
                alert("Impossible de récupérer créer le rendez-vous, erreur: " + error);
            }
        }
    }
}
