# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## [0.1.0](https://github.com/DSteph/lep-rdv-libre/releases/tag/v0.1.0) - 2023-11-14

### Added

- Mise en place de l'environnement de développemnt Vitejs avec le multi pages
- Prise en charge du token dans l'url

### Changed

- Installation de Alpine.js, Tailwind CSS et Flatpickr par npm
- Organisation générale des fichiers
- Gestion des variables de configuration dans .env géré par Vitejs


## [0.0.1](https://github.com/DSteph/lep-rdv-libre/releases/tag/v0.0.1) - 2023-10-11

### Added

- Mise en place des sources du projet fonctionnel de base
